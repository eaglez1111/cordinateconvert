import sys
import numpy as np
import matplotlib.pyplot as plt
from funcDef import *


## Data input
(t,rg,yaw,pch,x,y,z,leng)=readTs('TS.csv')
tht=126.4762 #Coordinate rotating angle

#print cosTht,sinTht
## Coordinate traslation - make the origin be the sensor tower
x1=x-x[0]
y1=y-y[0]

## Coordinate rotation - make the y-axis pass robot initial position
cosTht=np.cos(np.pi/180*tht)#-0.50980136993#
sinTht=np.sin(np.pi/180*tht)#0.86029213829#
x2=cosTht*x1+sinTht*y1
y2=cosTht*y1-sinTht*x1

## Getting bearing(yaw2) and range for new coordinate
yaw2=xy2angle(x2,y2)
rg2=np.sqrt( (x2*x2)+(y2*y2) )
print "done, coordinate convertion"

## Getting orientation angle of the robot
dx=np.zeros(leng)
dy=np.zeros(leng)
for i in range(3,leng-1):# (0):#
    print i
    m,n=1,1
    while (dis(x2[i],y2[i],x2[i-m],y2[i-m])<0.1) and (i-m>2):
        m=m+1
    while (dis(x2[i],y2[i],x2[i+n],y2[i+n])<0.1) and (i+n<leng-1):
        n=n+1
    dx[i]=x2[i+n]-x2[i-m]
    dy[i]=y2[i+n]-y2[i-m]
#(_t,_rg2,_yaw2,_x2,_y2,ang,_x3,_y3,_leng)=readTs2('TS_converted.csv')

dx[2],dx[-1]=dx[3],dx[-2]
dy[2],dy[-1]=dy[3],dy[-2]   # assign reasonable values

print "done, dx dy"
ang=xy2angle(dx,dy)
print "done, ang"

## Compensate the off-centering error
x3 = x2+np.cos(np.pi/180*ang)*0.13
y3 = y2+np.sin(np.pi/180*ang)*0.13
print "done, x3 y3"

## Save data
writeTs('TS_converted.csv',t,rg2,yaw2,x2,y2,ang,x3,y3,leng)

## Simulate IMU data
t=t[2:]
ang=ang[2:]
dAng=np.zeros(leng-2)
ddAng=np.zeros(leng-2)
dAng[1]=(ang[1]-ang[0])/(t[1]-t[0])
for i in range(2,leng-2):
    dAng[i] = (ang[i]-ang[i-1]) /(t[i]-t[i-1])
    ddAng[i] = (dAng[i]-dAng[i-1]) /(t[i]-t[i-1])
dAng[0]=dAng[1]
ddAng[0]=ddAng[2]
ddAng[1]=ddAng[2]
writeIMU('IMU_simulated.csv',t,ang,dAng,ddAng,leng-2)

if 0:
    pL=plt.subplot(121)
    pL.plot(x,y,'b.-',label='Total Station Cordinate')
    pL.legend()
    plt.ylabel('Y (meter)')
    plt.xlabel('X (meter)')

    pR=plt.subplot(122)
    pR.plot(x2,y2,'g.-',label='Sensor Tower Cordinate')
    plt.ylabel('Y (meter)')
    plt.xlabel('X (meter)')
    pR.legend()
    plt.show()

if 0:
    plt.plot(t,ang,'.',label='orientation')
    plt.plot(t,x2[2:]*10,'.-',label='x(m/10)')
    plt.ylabel('orientation (degree)')
    plt.xlabel('time (sec)')
    plt.legend()
    plt.show()

if 1:
    plt.plot(x2,y2,'.-',label='Before')
    plt.plot(x3,y3,'.-',label='After recentering')
    plt.ylabel('Y (meter)')
    plt.xlabel('X (meter)')
    plt.legend()
    plt.show()
