import numpy as np
import csv

def readTs(fileName):
    i=-1
    leng=sum(1 for line in open(fileName))+i
    with open(fileName,'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')

        t=np.zeros(leng)
        rg=np.zeros(leng)
        yaw=np.zeros(leng)
        pch=np.zeros(leng)
        x=np.zeros(leng)
        y=np.zeros(leng)
        z=np.zeros(leng)
        for row in spamreader:
            if i>=0:
                rg[i]=float(row[3])
                yaw[i]=float(row[4])
                pch[i]=float(row[5])
                x[i]=float(row[6])
                y[i]=float(row[7])
                z[i]=float(row[8])
                t[i]=(float(row[0])*60+float(row[1]))*60+float(row[2])

                if t[i]<t[0]:
                    t[i]=t[i]+60*60*24
            i=i+1

    t=t-t[0]
    return (t,rg,yaw,pch,x,y,z,leng)

#Origin 10.408,  50.51, 103.32, 1007.4174,2007.3007,2999.4572


def writeTs(fileName,t,rg2,yaw2,x2,y2,ang,x3,y3,leng):
    with open(fileName, mode='w') as spam:
        sw = csv.writer(spam, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        sw.writerow(['time(sec)','range(m)','yaw(deg)','x(m)','y(m)','orientation(deg)','x_recentered(m)','y_recentered(m)'])
        for i in range(leng):
            if i==0:
                sw.writerow([t[i],rg2[i],yaw2[i],x2[i],y2[i],'00','00','00','#sensor tower(origin)'])
            elif i==1:
                sw.writerow([t[i],rg2[i],yaw2[i],x2[i],y2[i],'00','00','00','#robot initial position(on y-axis)'])
            else:
                sw.writerow([t[i],rg2[i],yaw2[i],x2[i],y2[i],ang[i],x3[i],y3[i]])

def readTs2(fileName):
    i=-1
    leng=sum(1 for line in open(fileName))+i
    with open(fileName,'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')

        t=np.zeros(leng)
        rg2=np.zeros(leng)
        yaw2=np.zeros(leng)
        x2=np.zeros(leng)
        y2=np.zeros(leng)
        ang=np.zeros(leng)
        x3=np.zeros(leng)
        y3=np.zeros(leng)
        for row in spamreader:
            if i>=0:
                t[i]==float(row[0])
                rg2[i]=float(row[1])
                yaw2[i]=float(row[2])
                x2[i]=float(row[3])
                y2[i]=float(row[4])
                ang[i]=float(row[5])
                x3[i]=float(row[6])
                y3[i]=float(row[7])
            i=i+1
    return (t,rg2,yaw2,x2,y2,ang,x3,y3,leng)



def writeIMU(fileName,t,ang,dAng,ddAng,leng):
    with open(fileName, mode='w') as spam:
        sw = csv.writer(spam, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        sw.writerow(['time(sec)','Orientation Angle(deg)','Angular Vel.','Angular Acc.'])
        for i in range(leng):
            sw.writerow([t[i],ang[i],dAng[i],ddAng[i]])


def xy2angle(x,y):
    leng=len(x)
    ang=np.zeros(leng)
    for i in range(leng):
        if x[i]==0:
            x[i]=0.000001
        ang[i]=np.arctan(y[i]/x[i])/np.pi*180
        if x[i]<0 and y[i]>0:
            ang[i]=ang[i]+180
        if x[i]<0 and y[i]<0:
            ang[i]=ang[i]+180
        if x[i]>0 and y[i]<0:
            ang[i]=ang[i]+360
    return ang


def dis(x0,y0,x1,y1):
    ans = np.sqrt( (x1-x0)*(x1-x0) + (y1-y0)*(y1-y0) )
    #print '(',x0,y0,'), (',x1,y1,') -- ',ans
    return ans









#
